#!/bin/bash
# $PROJECT_DIR/BuglyTool/upload.sh 
if [[ $CONFIGURATION == "Release" ]]; then
	echo "CONFIGURATION = $CONFIGURATION"
	dSYMPath=$BUILD_DIR/Release-iphoneos/$TARGET_NAME.app.dSYM
	buildNumber=$(/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" "${PROJECT_DIR}/${INFOPLIST_FILE}")
	shortVersion=$(/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "${PROJECT_DIR}/${INFOPLIST_FILE}")
	bundleid=$(/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" "${PROJECT_DIR}/${INFOPLIST_FILE}")
	version=${shortVersion}"("${buildNumber}")"
	if [[ $bundleid == *PRODUCT_BUNDLE_IDENTIFIER* ]]; then
		#statements
		bundleid=$PRODUCT_BUNDLE_IDENTIFIER
	fi
	echo $version
	echo $bundleid
	cd $PROJECT_DIR/BuglyTool

	if [[ -d $dSYMPath ]]; then
		echo "dSYM正在上传"
		java -jar buglyqq-upload-symbol.jar -appid dd6f04bc7a -appkey 3d9c8449-5a90-42c6-bddc-e69f7c0f9c2d -bundleid $bundleid -version $version -platform IOS -inputSymbol $dSYMPath
	else
		echo "dSYM未上传"
	fi
	rm -rf buglybin
	rm -rf cp_buglySymboliOS.jar
	rm -rf cp_buglyQqUploadSymbolLib.jar
fi
